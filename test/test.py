import os

from bitbucket_pipes_toolkit.test import PipeTestCase

from bitbucket_pipes_toolkit import ArrayVariable

template = """
apiVersion: v1
kind: Service
metadata:
  name: my-nginx-svc
  labels:
    app: nginx
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: nginx
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80

"""


class KubeTestCase(PipeTestCase):

    maxDiff = None

    def setUp(self):
        super().setUp()
        with open('test/nginx.yml', 'w+') as nginx_yml:
            nginx_yml.write(template)

    def tearDown(self):
        super().tearDown()
        os.remove('test/nginx.yml')

    def test_apply(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "RESOURCE_PATH": 'test/nginx.yml',
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        })

        self.assertIn('kubectl apply was successful', result)

    def test_apply_with_default_labels(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "RESOURCE_PATH": 'test/nginx.yml',
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),
            "WITH_DEFAULT_LABELS": True,

            "BITBUCKET_STEP_TRIGGERER_UUID": "{asdasd-565656-asdad-565655}",

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        })

        self.assertIn('kubectl apply was successful', result)

        environment = {
            "KUBECTL_COMMAND": "get deployments",
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        }
        environment.update(ArrayVariable.from_list('KUBECTL_ARGS', ['--show-labels']).decompile())
        result = self.run_container(environment=environment)

        self.assertIn('bitbucket.org/bitbucket_repo_owner=atlassian', result)

    def test_apply_with_slashes_in_branch_label(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "RESOURCE_PATH": 'test/nginx.yml',
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "BITBUCKET_BRANCH": 'test/test',
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        })

        self.assertIn('WARNING: "/" is not allowed', result)

    def test_get_pods_successful(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "get pods",
            "KUBE_CONFIG": os.getenv('KUBE_CONFIG'),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY")
        })

        self.assertIn('✔ Pipe finished successfully!', result)
