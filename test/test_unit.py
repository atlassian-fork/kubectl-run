import os
import sys
import yaml
import base64
import shutil

from unittest import TestCase
from unittest import mock

from pipe.pipe import KubernetesDeployPipe
from bitbucket_pipes_toolkit import Pipe

template = """
apiVersion: v1
kind: Service
metadata:
  name: my-nginx-svc
  labels:
    app: nginx
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: nginx
"""


class CompletedProcessMock:
    def __init__(self, return_code):
        self.returncode = return_code


class KubectlBaseTestCase(TestCase):

    def setUp(self):
        self.schema = {
            'KUBE_CONFIG': {'type': 'string', 'required': True},
            'KUBECTL_COMMAND': {'type': 'string', 'required': True},
            'KUBECTL_ARGS': {'type': 'list', 'required': False, 'default': []},
            'RESOURCE_PATH': {'type': 'string', 'required': False, 'nullable': True, 'default': ''},
            'LABELS': {'type': 'list', 'required': False},
            'WITH_DEFAULT_LABELS': {'type': 'boolean', 'required': False, 'default': True},
            'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
        }

        with open('test/nginx.yml', 'w+') as nginx_yml:
            nginx_yml.write(template)

        # create fake base64 encoded kube config file
        data = 'config string'
        base64_data = base64.b64encode(data.encode('utf-8'))

        os.environ['BITBUCKET_STEP_TRIGGERER_UUID'] = "{asdasd-565656-asdad-565655}"

        os.environ['KUBE_CONFIG'] = str(base64_data, 'utf-8')
        os.environ['KUBECTL_COMMAND'] = 'apply'
        os.environ['RESOURCE_PATH'] = 'test/nginx.yml'

        self.pipe = KubernetesDeployPipe(schema=self.schema, check_for_newer_version=False)

    def tearDown(self):
        os.remove('test/nginx.yml')

        del os.environ['BITBUCKET_STEP_TRIGGERER_UUID']
        del os.environ['KUBECTL_COMMAND']
        del os.environ['RESOURCE_PATH']


class KubectlTestCase(KubectlBaseTestCase):

    def test_apply_success(self):
        with mock.patch('subprocess.run') as mock_run:
            mock_return_value = CompletedProcessMock(return_code=0)
            mock_run.return_value = mock_return_value
            self.pipe.run()
            mock_run.assert_called_with(['kubectl', 'apply', '-f', 'test/nginx.yml'], stdout=sys.stdout)

    def test_apply_with_default_labels(self):
        self.pipe.variables.update({'WITH_DEFAULT_LABELS': True})
        with mock.patch('subprocess.run') as mock_run:
            mock_return_value = CompletedProcessMock(return_code=0)
            mock_run.return_value = mock_return_value
            self.pipe.run()
            mock_run.assert_called_with(['kubectl', 'apply', '-f', 'test/nginx.yml'], stdout=sys.stdout)

            self.assertTrue(self.pipe.get_variable('WITH_DEFAULT_LABELS'))

    def test_apply_with_slashes_in_branch_label(self):
        os.environ['BITBUCKET_BRANCH'] = "test/test"

        with mock.patch.object(Pipe, 'log_warning') as mock_warning:
            mock_warning.return_value = None
            with mock.patch('subprocess.run') as mock_run:
                mock_return_value = CompletedProcessMock(return_code=0)
                mock_run.return_value = mock_return_value
                self.pipe.run()

                mock_run.assert_called_with(['kubectl', 'apply', '-f', 'test/nginx.yml'], stdout=sys.stdout)
                mock_warning.assert_called_with('"/" is not allowed in kubernetes labels. Slashes will be replaced by a dash "-" '
                                                'in the "bitbucket.org/bitbucket_commit" label value.For more information you can '
                                                'check the official kubernetes docshttps://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set')

        del os.environ['BITBUCKET_BRANCH']

    def test_update_labels_in_metadata_add_label(self):
        labels = {
            "environment": "test"
        }
        template = self.pipe.get_variable('RESOURCE_PATH')
        self.pipe.update_labels_in_metadata(template, labels)

        with open(template, 'r') as template_file:
            yaml_data = yaml.safe_load(template_file.read())
            updated_labels = yaml_data['metadata']['labels']
            expected_label = "environment"

            self.assertIn(expected_label, updated_labels)

    def test_update_labels_in_metadata_new_label_not_break_existing(self):
        labels = {
            "environment": "test"
        }
        template = self.pipe.get_variable('RESOURCE_PATH')
        self.pipe.update_labels_in_metadata(template, labels)

        with open(template, 'r') as template_file:
            yaml_data = yaml.safe_load(template_file.read())
            updated_labels = yaml_data['metadata']['labels']
            existing_label = "app"

            self.assertIn(existing_label, updated_labels)

    def test_update_labels_step_triggerer_trims_curly_braces(self):

        template = self.pipe.get_variable('RESOURCE_PATH')

        with mock.patch('subprocess.run') as mock_run:
            mock_return_value = CompletedProcessMock(return_code=0)
            mock_run.return_value = mock_return_value
            self.pipe.run()

        with open(template, 'r') as template_file:
            yaml_data = yaml.safe_load(template_file.read())
            updated_labels = yaml_data['metadata']['labels']

            actual_step_triggerer_uuid = updated_labels['bitbucket.org/bitbucket_step_triggerer_uuid']
            expected_step_triggerer_uuid = "asdasd-565656-asdad-565655"

            self.assertEqual(actual_step_triggerer_uuid, expected_step_triggerer_uuid)

    def test_update_labels_in_metadata_empty_labels(self):
        template = self.pipe.get_variable('RESOURCE_PATH')

        with open(template, 'r') as template_file:
            yamls_start = yaml.safe_load(template_file.read())

        self.pipe.update_labels_in_metadata(template, labels={})

        with open(template, 'r') as template_file:
            yamls_after = yaml.safe_load(template_file.read())

        self.assertEqual(yamls_start, yamls_after)

    def test_update_labels_in_metadata_empty_metadata(self):
        template = """
        apiVersion: v1
        kind: Service
        """
        with open('test/no_metadata.yml', 'w+') as f:
            f.write(template)

        with self.assertRaisesRegex(KeyError, "metadata"):
            self.pipe.update_labels_in_metadata("test/no_metadata.yml", labels={})


class KubectlDeployDirectoryTestCase(KubectlBaseTestCase):

    def setUp(self):
        super().setUp()

        os.mkdir('test/test-dir')

        with open('test/test-dir/nginx.yml', 'w+') as nginx_yml:
            nginx_yml.write(template)

        os.environ['RESOURCE_PATH'] = 'test/test-dir/'
        os.environ['BITBUCKET_REPO_OWNER'] = 'test-owner'

    def test_update_labels_in_metadata_add_label(self):
        pipe = KubernetesDeployPipe(schema=self.schema, check_for_newer_version=False)

        pipe.variables.update({'WITH_DEFAULT_LABELS': True})
        with mock.patch('subprocess.run') as mock_run:
            mock_return_value = CompletedProcessMock(return_code=0)
            mock_run.return_value = mock_return_value
            pipe.run()
            mock_run.assert_called_with(['kubectl', 'apply', '-f', 'test/test-dir/'], stdout=sys.stdout)

            self.assertTrue(pipe.get_variable('WITH_DEFAULT_LABELS'))

        with open('test/test-dir/nginx.yml', 'r') as template_file:
            yaml_data = yaml.safe_load(template_file.read())
            updated_labels = yaml_data['metadata']['labels']
            expected_label = "bitbucket.org/bitbucket_repo_owner"

            self.assertIn(expected_label, updated_labels)

    def tearDown(self):
        shutil.rmtree('test/test-dir')
        del os.environ['KUBECTL_COMMAND']
        del os.environ['RESOURCE_PATH']
        del os.environ['BITBUCKET_REPO_OWNER']
