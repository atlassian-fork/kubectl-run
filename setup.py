from distutils.core import setup

setup(
    name='kubectl_run',
    version='1.1.3',
    packages=['kubectl_run', ],
    package_dir={'kubectl_run': 'pipe'},
    url='https://bitbucket.org/atlassian/kubectl-run',
    author='Atlassian',
    author_email='bitbucketci-team@atlassian.com',
    description='Pipe kubectl-run',
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    install_requires=[
        'bitbucket-pipes-toolkit==1.12.0',
        'awscli==1.16.279'

    ]
)
